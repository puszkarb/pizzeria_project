package DBControl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class ObslugaBD {
    private static final String jdbcDriver = "com.mysql.jdbc.Driver";

    public static Connection polaczZBaza(){
        System.out.println("Jedziemy z polaczeniem");
        try
        {
            Class.forName(jdbcDriver);
            System.out.println("JDBC driver loaded");
        }
        catch (Exception err)
        {
            System.err.println("Error loading JDBC driver");
            err.printStackTrace(System.err);
            System.exit(0);
        }
        Connection databaseConnection= null;
        try
        {
            //Connect to the database
            databaseConnection = DriverManager.getConnection("jdbc:mysql://localhost:3306/pizzeria?useUnicode=yes&characterEncoding=UTF-8&useSSL=false","pizzeria","pizzeria");
            System.out.println("Connected to the database");
            return databaseConnection;
        }
        catch (SQLException err)
        {
            System.err.println("Error connecting to the database");
            err.printStackTrace(System.err);
            System.exit(0);
            return null;
        }

    }


}