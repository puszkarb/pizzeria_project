package DBControl;

import Products.Order;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class ObslugaBDZamowienia extends ObslugaBD {

    public static void dodajZamowienieDoBazy(Order order){
        try {
            Connection conn = polaczZBaza();
            Statement sqlStat = conn.createStatement();
            String query = "insert into pizzeria.zamowienia(pizza_sklad,sosy,napoje,ulica,telefon"
                    + ",status, komentarz,iloscPkt,cena) values('" + order.dobazypizza()+ "','" + "SOS" + "','" + order.dobazynapoj()
                    + "','" + "ADRES" + "','" + "TEL" + "','" + order.getStatus() + "','"
                    + order.getKomentarz() + "','" + order.getPkt() + "','" + order.getPrice()  + "')";
            System.out.println(query);
            sqlStat.executeUpdate(query);
            ResultSet rs = sqlStat.executeQuery("select max(p.id_zamowienia) from pizzeria.zamowienia p");
            rs.next();
            order.setOrder_id(rs.getInt(1));
            System.out.println("Dodano zamowienie o numerze " + order.getId());
            conn.close();
        }
        catch (SQLException ex){
            System.err.println("Error connecting to the database3");
            ex.printStackTrace(System.err);
            System.exit(0);
        }

    }

    public static void zmienStanZamowienia(Order order, String nowyStan){
        try {
            Connection conn = polaczZBaza();
            Statement sqlStat = conn.createStatement();
            String query = "update pizzeria.zamowienia set status='" + nowyStan +"' where id_zamowienia = " + order.getId();
            sqlStat.executeUpdate(query);
            order.changeStatus(nowyStan);
            System.out.println("Zmieniono status zamowienia o id " + order.getId() + " na " + order.getStatus());
            conn.close();
        }catch (SQLException ex){
            System.err.println("Error connecting to the database3");
            ex.printStackTrace();
            System.exit(0);
        }
    }
/*
    public static List<Order> pokazHistorieZamowien(Client klient){
        List<Order> listaZamowien = new ArrayList<>();
        try{
            Connection conn = polaczZBaza();
            Statement sqlStat = conn.createStatement();
            String query = "select * from pizzeria.zamowienia z where z.email_zamawiacza = '" + klient.getEmail() + "'";
            ResultSet rs = sqlStat.executeQuery(query);
            while(rs.next()){
                Order zam = new Order(rs.getInt("id_zamowienia"),rs.getDouble("cena"), rs.getInt("iloscPkt"),rs.getString("komentarz"), rs.getString("status") );
                listaZamowien.add(zam);
            }
        }catch (SQLException ex){
            System.err.println("Error connecting to the database in pokazHistorieZamowien");
            ex.printStackTrace();
            System.exit(0);
        }
        return listaZamowien;
    } */
}
