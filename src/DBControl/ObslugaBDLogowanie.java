package DBControl;

import Server_client.User;

import java.sql.*;

public class ObslugaBDLogowanie extends ObslugaBD {

    public static boolean dodajUzytkownia(String email, String haslo, String ulica, String tel) {
        boolean czyUdaloSieDodac = false;
        try {
            Connection conn = polaczZBaza();
            Statement sqlStat = conn.createStatement();

            String query = "insert into pizzeria.dane_klientow(email, haslo, ulica, telefon, salt) values(?,?,?,?,?)";
            String salt = SzyfrowanieHasla.getSalt(40);
            PreparedStatement prSt = conn.prepareStatement(query);
            prSt.setString(1, email);
            prSt.setString(2, SzyfrowanieHasla.generateSecurePassword(haslo, salt));
            prSt.setString(3, ulica);
            prSt.setString(4, tel);
            prSt.setString(5, salt);
            prSt.execute();
            System.out.println("Dodano uzytkownika");
            czyUdaloSieDodac = true;
            conn.close();


        }catch (SQLException ex){
            System.err.println("Error connecting to the database1");
            ex.printStackTrace(System.err);
            System.exit(0);
        }
        return czyUdaloSieDodac;

    }

   public static boolean sprawdzCzyJestWBazie(String email, String haslo){
        boolean czyZiomekJestWBD = false;
        try {
            Connection conn = polaczZBaza();
            Statement sqlStat = conn.createStatement();
            ResultSet rs = sqlStat.executeQuery("select p.email, p.haslo, p.salt from pizzeria.dane_klientow p where p.email = '" + email + "'");
            if (rs.next()){
                String emailZBazy = rs.getString("email");
                String hasloZBazy = rs.getString("haslo");
                String saltZBazy = rs.getString("salt");
                if(emailZBazy.equals(email) && SzyfrowanieHasla.verifyUserPassword(haslo,hasloZBazy,saltZBazy)){
                    czyZiomekJestWBD = true;
                    System.out.println("Poprawna weryfikacja - osoba jest w bazie");
                    return czyZiomekJestWBD;
                }
            }
            conn.close();
        }catch (SQLException ex){
            System.err.println("Error connecting to the database when sprawdzCzyJestWBazie");
            ex.printStackTrace(System.err);
            System.exit(0);
        }
        System.out.println("Tej osoby nie ma w bazie");
        return czyZiomekJestWBD;

    }

    public static User getUserFromDatabase( String email ){
        User returnedUser = new User();
        return returnedUser;
    }
}
