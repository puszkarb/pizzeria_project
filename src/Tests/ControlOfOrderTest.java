package Tests;

import org.junit.jupiter.api.Test;
import Products.ControlOfOrder;
import Products.Pizza;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static Products.ControlOfOrder.getOrder;

public class ControlOfOrderTest {

    static Pizza pizza;
    static ControlOfOrder controlOfOrder = new ControlOfOrder();

    @Test
    void createPizzaTest(){
        controlOfOrder.createPizza("Testowa Pizza");
        assertTrue(controlOfOrder.getPizza().getName()=="Testowa Pizza");
    }

    @Test
    void addExtraIngridientsTest(){
        controlOfOrder.createPizza("Testowa Pizza");
        controlOfOrder.addExtraIngredients(true,true,true,true,true,true,true);
        assertTrue(controlOfOrder.getPizza().getPieczarki()==2);
        assertTrue(controlOfOrder.getPizza().getSer()==2);
        assertTrue(controlOfOrder.getPizza().getAnanas()==2);
        assertTrue(controlOfOrder.getPizza().getBekon()==2);
        assertTrue(controlOfOrder.getPizza().getKukurydza()==2);
        assertTrue(controlOfOrder.getPizza().getPomidory()==2);

    }

    @Test
    void addPizzaToOrderTest(){
        controlOfOrder.createPizza("Testowa Pizza");
        controlOfOrder.addPizzaToOrder();
        assertTrue(getOrder().getProduct(0) instanceof Pizza);
    }

}
