package Tests;

import DBControl.ObslugaBDLogowanie;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ObslugaBDLogowanieTest {

    @Test
    void sprawdzCzyJestWBazie1() {
        assertFalse(ObslugaBDLogowanie.sprawdzCzyJestWBazie("Email ktorego nie w bd" ,"qwer"));
    }

    @Test
    void sprawdzCzyJestWBazie2() {
        assertFalse(ObslugaBDLogowanie.sprawdzCzyJestWBazie("wlodarczyk_pawel1@wp.pl" ,"zle haslo"));
    }

    @Test
    void sprawdzCzyJestWBazie3() {
        assertTrue(ObslugaBDLogowanie.sprawdzCzyJestWBazie("mail@pl.pl" ,"qwerty"));
    }
}