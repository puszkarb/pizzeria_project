package Tests;

import org.junit.jupiter.api.Test;
import Server_client.Server;

import java.io.IOException;

class ServerTest {
    @Test
    void Test_server() throws IOException {

        final int serverPort=6969;
        Server server = new Server(serverPort);
        System.out.println("Oczekiwane uruchamienie serwera oraz komunikat o gotowności połączenia");
        server.run();
    }


}