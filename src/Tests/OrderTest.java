package Tests;


import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import Products.Napoj;
import Products.Order;
import Products.Pizza;


import static org.junit.jupiter.api.Assertions.assertTrue;

public class OrderTest {

    static Order order= new Order( 0,"z1", "test", "nowe");
    static Pizza pizza = new Pizza("Elektryzujaca", 150 );
    static Napoj woda = new Napoj( "woda" );


    @BeforeAll
    static void setUp(){
        order.addProduct(pizza);
        order.addProduct(woda);
        System.out.println(order.dobazynapoj());
    }

    @Test
    void testGetProduct() {
        assertTrue(order.getProduct(0).equals(pizza));
        assertTrue(order.getProduct(1).equals(woda));
    }

    @Test
    void testHowManyProducts() {
        assertTrue(order.howManyProducts() == 2);
    }

    @Test
    void testDoBazyPizza() {
        String s1 = order.dobazypizza();
        String s2 = "Elektryzujaca ser: 2 pomidory: 1 pieczarki: 0 szynka: 1 ananas: 1 kukurydza: 1 bekon: 0 ciasto: 0 rozmiar: 150 ";
        assertTrue(s1.equals(s2));
    }

    @Test
    void testDoBazyNapoj() {
        String s1 = order.dobazynapoj();
        String s2 = "woda pojemnosc: 1 lod: true ";
        assertTrue(s1.equals(s2));
    }


}
