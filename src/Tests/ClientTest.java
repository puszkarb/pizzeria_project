package Tests;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import Server_client.Client;
import Server_client.User;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class ClientTest {
    static Client client;
    private static User user;
    private static char[] array= {'a','b','c','d','e','f','g','h','i','j','k','l','z','x','c','v','b','n','m'};
    private static char  getRandomCharacter(){
        return array[random(array.length)];
    }
    private static int random(int length){
        return new Random().nextInt(length);
    }
     private static   String clientTest(){
        boolean running=true;
        int count= 0;
        int max = 1;
        StringBuilder sb = new StringBuilder();
        while (running){
            int size=random(5);
            for (int i=0;i<size;i++){
                sb.append(getRandomCharacter());
            }
            if(count++==max){
                running=false;
            }
        }
        return sb.toString();
    }
    @BeforeAll
    static void setUp(){
        String login= clientTest();
        String haslo= clientTest();
        String ulica= clientTest();
        String tel =clientTest();
        String email=clientTest();
        client=new Client("localhost", 6969);
        user = new User(login,haslo,ulica,tel,email);

    }
    @Test
    void zatwierdzUser() {
        client.connect();
        assertTrue((client.zatwierdzUser(user)));
        System.out.println("Próba rejestracji  użytkownika który jest już w bazie");
        assertFalse((client.zatwierdzUser(user)));
    }
}