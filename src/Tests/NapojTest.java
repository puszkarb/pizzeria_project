package Tests;

import org.junit.jupiter.api.Test;
import Products.Napoj;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class NapojTest {
    static Napoj testSevenupa= new Napoj("Sevenupduza");
    static Napoj testPepsi= new Napoj("Pepsimala");

    @Test
    void checkNameTest() {
        assertTrue(testSevenupa.getName() == "Sevenupduza");
        assertTrue(testPepsi.getName() == "Pepsimala");
        assertFalse(testSevenupa.getName() == "Pepsimala");
    }

    @Test
    void addLodTest() {
            testSevenupa.setLod(true);
            assertTrue(testSevenupa.getLod() == true);
            assertFalse(testPepsi.getLod() == false);
    }

    @Test
    void checkPojemnoscTest() {
        assertTrue(testSevenupa.getPojemnosc()==1);
        assertTrue(testPepsi.getPojemnosc()==500);
        assertFalse(testPepsi.getPojemnosc()==1);

        testPepsi.setPojemnosc(6969);
        assertTrue(testPepsi.getPojemnosc()==6969);
    }
}
