package Tests;

import DBControl.EmailValidator;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class EmailValidatorTest {

    static EmailValidator emailValidator;
    @BeforeAll
    static void setUp(){
        emailValidator = new EmailValidator();
    }

    @Test
    void validateEmail1() {
        assertTrue(emailValidator.validateEmail("pawel_wlodarczyk@gmail.com"));
    }

    @Test
    void validateEmail2() {
        assertTrue(emailValidator.validateEmail("pawel_wlodarczyk@wp.pl"));
    }

    @Test
    void validateEmail3() {
        assertTrue(emailValidator.validateEmail("pawel.wlodarczyk@gmail.com"));
    }

    @Test
    void validateEmail4() {
        assertFalse(emailValidator.validateEmail("tenEmajlJestZly.pl"));
    }

    @Test
    void validateEmail5() {
        assertFalse(emailValidator.validateEmail("tenEmajlJestZlypl2"));
    }

    @Test
    void validateEmail6() {
        assertFalse(emailValidator.validateEmail("tennibydobry@alebezkropkapl"));
    }

    @Test
    void validateEmail7() {
        assertFalse(emailValidator.validateEmail("tennibydobryzrkopkoalebezmalpy.pl"));
    }

    @Test
    void validateEmail8() {
        assertFalse(emailValidator.validateEmail(""));
    }

    @Test
    void validateEmail9() {
        assertFalse(emailValidator.validateEmail(" "));
    }

    @Test
    void validateEmail10() {
        assertFalse(emailValidator.validateEmail("email ze spacjami @ gmail. com"));
    }

    @Test
    void validateEmail11() {
        assertFalse(emailValidator.validateEmail("emailze/znakam/\\ispecjalnymi@wp.pl"));
    }
    @Test
    void validateEmail12() {
        assertFalse(emailValidator.validateEmail(".........@...,.pl"));
    }
}