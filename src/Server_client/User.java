package Server_client;

import java.io.Serializable;

public class User implements Serializable {

    String login;
    String haslo;

    String ulica;
    String tel;
    String email;

    public User(String login, String haslo, String ulica, String tel, String email) {
        this.login = login;
        this.haslo = haslo;
        this.ulica = ulica;
        this.tel = tel;
        this.email = email;
    }
    public User (String login, String haslo){
        this.email=login;
        this.haslo=haslo;
    }

    public User() {
    }


    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getHaslo() {
        return haslo;
    }

    public void setHaslo(String haslo) {
        this.haslo = haslo;
    }

    public String getUlica() {
        return ulica;
    }

    public void setUlica(String ulica) {
        this.ulica = ulica;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString(){
        return "\t\tLogin: "  + getLogin() + " \n\t\t" + "Hasło: "  + getHaslo() + " \n\t\t" + "Telefon: "  + getTel() + " \n\t\t" + "Adres: "  + getUlica() + " \n\t\t" +"Email: "  + getEmail() + " \n" ;
    }
}

