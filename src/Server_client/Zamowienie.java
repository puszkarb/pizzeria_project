package Server_client;

import java.io.Serializable;


public class Zamowienie implements Serializable{


    private int id;
    private double cena;
    private int iloscPkt;
    private String komentarz;
    private String status;

    public Zamowienie(int id, double cena, int iloscPkt, String komentarz, String status) {
        this.id = id;
        this.cena = cena;
        this.iloscPkt = iloscPkt;
        this.komentarz = komentarz;
        this.status = status;


    }


    public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public double getCena() {
            return cena;
        }

        public void setCena(double cena) {
            this.cena = cena;
        }

        public int getIloscPkt() {
            return iloscPkt;
        }

        public void setIloscPkt(int iloscPkt) {
            this.iloscPkt = iloscPkt;
        }

        public String getKomentarz() {
            return komentarz;
        }

        public void setKomentarz(String komentarz) {
            this.komentarz = komentarz;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        @Override
        public String toString(){
        return "Order: \n\tID:  " + id+ " \n\tPunkty: " + iloscPkt +  "\n\tKomentarz: " + komentarz + "\n\tStatus: " + status;

        }




}
