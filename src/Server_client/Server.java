package Server_client;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Server extends Thread {

    private final int serverPort;
    private ArrayList<ServerWorker> workerList = new ArrayList<>();


    public Server(int serverPort) {
        this.serverPort = serverPort;
    }

    public List<ServerWorker> getWorkerList(){
        return this.workerList;
    }

    public void run(){
        try{
            ServerSocket serverSocket = new ServerSocket(this.serverPort);

            while(true){
                System.out.println("Gotowy do połączenia z nowym klientem...");
                Socket clientSocket = serverSocket.accept();
                System.out.println("Połączono z nowym klientem ...");

                ServerWorker worker = new ServerWorker(this, clientSocket);
                this.workerList.add(worker);
                worker.start();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean removeWorker(ServerWorker serverWorker){
        this.workerList.remove(serverWorker);
        return true;

    }
    public void addToWorkerList(ServerWorker serverWorker){
        this.workerList.add(serverWorker);
    }
}
