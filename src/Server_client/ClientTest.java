package Server_client;

import javafx.application.Application;
import GUI.Main;

public class ClientTest {

    public static void main(String[] args) {

        Client klient = new Client("localhost", 6969);
        klient.connect();

        Application.launch(Main.class, args);

    }
}
