package Server_client;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client extends  User {

    private final String serverName;
    private final int severPort;
    private Socket socket;
    private static InputStream serverIn;
    private static OutputStream serverOut;
    private BufferedReader bufferedIn;
    private static ObjectOutputStream objectOutputStreamToServer;
    private static ObjectInputStream objectInputStream;
    private int iloscPkt = (int) (Math.random() * 20);

    public Client(String serverName, int severPort) {
        this.serverName = serverName;
        this.severPort = severPort;
    }



    public void connect() {
        try {

            this.socket = new Socket(this.serverName, this.severPort);
            System.out.println("Client port is: " + this.socket.getLocalPort());

            this.serverOut = this.socket.getOutputStream();
            System.out.println("OutputStream ok");

            this.serverIn = this.socket.getInputStream();
            System.out.println("InputStream ok");


            this.objectOutputStreamToServer = new ObjectOutputStream(serverOut);
            System.out.println("ObjectOutputStream ok");

    objectOutputStreamToServer.flush();



            this.objectInputStream = new ObjectInputStream(serverIn);
            System.out.println("ObjectInputStream ok");


        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static Zamowienie noweZamowienie(Client klient) { //trzeba dodać jeszcze produkt
        return new Zamowienie( (int)(Math.random()*100), 20.1, klient.getIloscPkt(), "Witajcie Serwerze!", "iście testowane zamowienie");
    }


   public static void zatwierdzZamowienie(Zamowienie order) throws IOException {
        objectOutputStreamToServer.writeObject(order);
    }

    public static boolean zatwierdzUser(User newUser) {
        try {
            objectOutputStreamToServer.writeObject(newUser);

            boolean registeredSuccessfully;
            registeredSuccessfully = (boolean) objectInputStream.readObject();
            if (registeredSuccessfully)
                return true;
            else
                return false;
        } catch (IOException | ClassNotFoundException e) {

            return false;
        }

    }




    private int getIloscPkt() {
        return iloscPkt;
    }


    public InputStream getServerIn(){
        return serverIn;
    }


}

