package Products;

public class Product
{

    private String name;
    private double price;
    private int pkt;
    private String notes;

    public boolean checkForAvailability(){
        if(name == null && name.isEmpty()){
            return true;
        }else {
            return false;
        }
    }
    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Product( String name, double price, int pkt )
    {
        this.name = name;
        this.price = price;
        this.pkt = pkt;
        notes = "";
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public int getPkt() {
        return pkt;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString()
    {
        StringBuilder outText = new StringBuilder();
        outText.append(name).append(" ").append(price);
        return outText.toString();
    }
}
