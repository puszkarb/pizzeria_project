package Products;

public class Pizza extends Product {

    private int ser;
    private int pomidory;
    private int pieczarki;
    private int szynka;
    private int ananas;
    private int kukurydza;
    private int bekon;
    private int ciasto;
    private int rozmiar;

    public Pizza( String name, int size )
    {

        super( name, 5, 1);
        this.rozmiar = size;
        switch (name) {
            case "Studencka":
                ser = 1;
                pomidory = 1;
                pieczarki = 0;
                szynka = 0;
                ananas = 0;
                kukurydza = 0;
                bekon = 0;
                break;

            case "Wezuwiusz":
                ser = 2;
                pomidory = 1;
                pieczarki = 0;
                szynka = 1;
                ananas = 0;
                kukurydza = 0;
                bekon = 0;
                break;


            case "Nowowiejska":
                ser = 1;
                pomidory = 1;
                pieczarki = 0;
                szynka = 0;
                ananas = 0;
                kukurydza = 0;
                bekon = 1;
                break;

            case "Elektryzujaca":
                ser = 2;
                pomidory = 1;
                pieczarki = 0;
                szynka = 1;
                ananas = 1;
                kukurydza = 1;
                bekon = 0;
                break;

            default:
                ser = 1;
                pomidory = 1;
                pieczarki = 1;
                szynka = 1;
                ananas = 1;
                kukurydza = 1;
                bekon = 1;
                break;
        }

    }


    public int getRozmiar() {
        return rozmiar;
    }

    public void setRozmiar(int rozmiar) {
        this.rozmiar = rozmiar;
    }

    public int getPomidory() {
        return pomidory;
    }

    public void setPomidory(int pomidory) {
        this.pomidory = pomidory;
    }

    public int getPieczarki() {
        return pieczarki;
    }

    public void setPieczarki(int pieczarki) {
        this.pieczarki = pieczarki;
    }

    public int getSer() {
        return ser;
    }

    public void setSer(int ser) {
        this.ser = ser;
    }

    public int getSzynka() {
        return szynka;
    }

    public void setSzynka(int szynka) {
        this.szynka = szynka;
    }

    public int getAnanas() {
        return ananas;
    }

    public void setAnanas(int ananas) {
        this.ananas = ananas;
    }

    public int getKukurydza() {
        return kukurydza;
    }

    public void setKukurydza(int kukurydza) {
        this.kukurydza = kukurydza;
    }

    public int getBekon() {
        return bekon;
    }

    public void setBekon(int bekon) {
        this.bekon = bekon;
    }

    public int getCiasto() {
        return ciasto;
    }

    public void setCiasto(int ciasto) {
        this.ciasto = ciasto;
    }

    @Override
    public String toString()
    {
        StringBuilder sklad = new StringBuilder();
        sklad.append(getName()).append(" ");
        sklad.append("ser: ").append(ser).append(" ");
        sklad.append("pomidory: ").append(pomidory).append(" ");
        sklad.append("pieczarki: ").append(pieczarki).append(" ");
        sklad.append("szynka: ").append(szynka).append(" ");
        sklad.append("ananas: ").append(ananas).append(" ");
        sklad.append("kukurydza: ").append(kukurydza).append(" ");
        sklad.append("bekon: ").append(bekon).append(" ");
        sklad.append("ciasto: ").append(ciasto).append(" ");
        sklad.append("rozmiar: ").append(rozmiar).append(" ");
        return  sklad.toString();
    }
}
