package Products;

public class Napoj extends Product {

    private int pojemnosc;
    private Boolean lod;

    public Napoj ( String name )
    {

        super( name, 5, 1);
        switch (name) {
            case "Pepsimala":
                pojemnosc=500;
                lod=true;
                break;

            case "Pepsiduza":
                pojemnosc=1;
                lod=true;
                break;


            case "Sevenupmala":
                pojemnosc=500;
                lod=true;
                break;

            case "Sevenupduza":
                pojemnosc=1;
                lod=true;
                break;

            default:
                pojemnosc=1;
                lod=true;
                break;
        }

    }





    /*public Napoj( String nazwa, double cena, int pkt )
    {
        super( nazwa, cena, pkt);
    }*/

    public void setPojemnosc( int pojemnosc ) {
        this.pojemnosc = pojemnosc;
    }

    public int getPojemnosc() {
        return pojemnosc;
    }

    public Boolean getLod() {
        return lod;
    }

    public void setLod(Boolean lod) {
        this.lod = lod;
    }

    @Override
    public String toString()
    {
        StringBuilder sklad = new StringBuilder();
        sklad.append(getName()).append(" ");
        sklad.append("pojemnosc: ").append(pojemnosc).append(" ");
        sklad.append("lod: ").append(lod).append(" ");

        return  sklad.toString();
    }

}
