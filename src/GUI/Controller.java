package GUI;

import GUI.Controllers.BaseController;
import DBControl.EmailValidator;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import Server_client.User;
import Server_client.Zamowienie;

import java.io.IOException;

import static Server_client.Client.zatwierdzUser;
import static Server_client.Client.zatwierdzZamowienie;
public class Controller {

    private Main application;
    private Scene scene;

    public void configure(final Main application, final Scene scene) {
        this.application = application;
        this.scene = scene;
    }

    public void show() {
        application.show(scene);
    }

    public void doGlownej() {
        application.controller1.show();
    }

    public void doRejestracji() {
        application.controller2.show();
    }

    public void doWyboruRozmiaru() throws IOException {
        final FXMLLoader loader = new FXMLLoader(getClass().getResource("Rozmiar.fxml"));
        loader.load();
        final BaseController controller = loader.getController();
        controller.configure(application, new Scene(loader.getRoot()));
        controller.show();
    }

    public void doWyboruCiasta() {
        application.controller4.show();
    }

    public void udanaRejestracja() {
        application.controller5.show();
    }

    public void nieZarejestruj() {
        application.controller6.show();
    }

    public void nieZaloguj() {
        application.controller7.show();
    }

    public void doWyboruSzablonu() {
        application.controller8.show();
    }

    public void doWyboruSkladnikow() {
        application.controller9.show();
    }

    public void doWidokuPracownika() throws IOException {
        final FXMLLoader loader = new FXMLLoader(getClass().getResource("Worker.fxml"));
        loader.load();
        final BaseController controller = loader.getController();
        controller.configure(application, new Scene(loader.getRoot()));
        controller.show();

    }


    //Order order= new Order(1,10,10,"Z kontrolerka", "syra" ); kurczaki az mi glupio wywalac

    @FXML
    private JFXTextField emailTextField;
    @FXML
    private JFXPasswordField passwordTextField;
    @FXML
    private JFXTextField telefonTextField;
    @FXML
    private JFXTextField adresTextField;
    @FXML
    private JFXPasswordField passwordLog;
    @FXML
    private JFXTextField emaiLog;


    //public void send(){ application.controller1.sendOrder(order);}  //prehistoria
    public void sendOrder(Zamowienie order) {

        try {
            zatwierdzZamowienie(order);
        } catch (IOException e) {
            System.out.println("Nie udało się wysłać zamówienia");
        }
    }


    public void handleUserRegistration() {
        EmailValidator emailValidator = new EmailValidator();
        if(emailValidator.validateEmail(emailTextField.getText())) {
            User newUser = new User(emailTextField.getText(), passwordTextField.getText(), adresTextField.getText(), telefonTextField.getText(), emailTextField.getText());

            System.out.println(newUser.toString());


            if (zatwierdzUser(newUser)) {
                //application.controllerErrorDuringRegistration.show();
                System.out.println("Udało się przeprowadzic proces rejestracji użytkownika");
                udanaRejestracja();
            } else {
                //application.controllerSuccessfulRegistration.show();
                System.out.println("Nie udało się przeprowadzic procesu rejestracji użytkownika");
                nieZarejestruj();
            }
        }else {
            System.out.println("Email o podanym formacie jest niepoprawny");
            nieZarejestruj();
        }
    }



    public void handLogowanie() throws IOException {

        if (emaiLog.getText().equalsIgnoreCase("worker") && passwordLog.getText().equals("worker")){
            System.out.println("Sprawdzono ze worker");
            doWidokuPracownika();
        }else {

            User existingUser = new User(emaiLog.getText(), passwordLog.getText());


            System.out.println("logowanie usera " + existingUser.getEmail() + " " + existingUser.getHaslo());
            if (zatwierdzUser(existingUser)) {
                System.out.println("Zalogowaliśmy");

                doWyboruRozmiaru();
            } else {
                System.out.println("Nie zalogowaliśmy");

                nieZaloguj();
            }
        }

    }



}