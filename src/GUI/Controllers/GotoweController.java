package GUI.Controllers;

import DBControl.ObslugaBDZamowienia;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import Products.ControlOfOrder;

public class GotoweController extends BaseController {

    @FXML
    private JFXButton EndButton;

    @FXML
    private JFXButton nextPizzaButton;

    @FXML
    private JFXButton addDrinkButton;

    @FXML
    private JFXTextArea poleDoZamowienia;



    public static ControlOfOrder getControllerOfOrder() {
        return controllerOfOrder;
    }

    public void initialize()
    {
        poleDoZamowienia.setText(controllerOfOrder.getOrder().toString() + "\n" + "\n"  +  controllerOfOrder.getOrder().getProduct(0).toString() + controllerOfOrder.getOrder().getProduct(controllerOfOrder.getOrder().howManyProducts()-1).toString());
    }


    public void sendToDB() {


            ObslugaBDZamowienia.dodajZamowienieDoBazy(ControlOfOrder.getOrder());

    }

    public void Test(ActionEvent event )   throws java.io.IOException
    {
        /*
        final FXMLLoader loader = new FXMLLoader(getClass().getResource("../wybierzSkladniki.fxml"));
        loader.load();
        final BaseController controller = loader.getController();
        controller.configure(application, new Scene(loader.getRoot()));
        controller.setPreviousController(this);
        controller.show(); */
    }
}
