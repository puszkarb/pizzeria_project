package GUI.Controllers;

import com.jfoenix.controls.JFXButton;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import Products.ControlOfOrder;

public class CiastoController extends BaseController {


    public static ControlOfOrder getControllerOfOrder() {
        return controllerOfOrder;
    }

    @FXML
    private JFXButton cienkieCiastoButton;
    @FXML
    private JFXButton grubeCiastoButton;


    public void wybierzCiasto( ActionEvent event ) throws java.io.IOException
    {
        if ( event.getSource().equals((cienkieCiastoButton)))
        {
            controllerOfOrder.PizzaSetCiasto(1);
        }
        else if(event.getSource().equals(grubeCiastoButton)) {
            controllerOfOrder.PizzaSetCiasto(2);
        }

        final FXMLLoader loader = new FXMLLoader(getClass().getResource("../wybierzSzablon.fxml"));
        loader.load();
        final BaseController controller = loader.getController();
        controller.configure(application, new Scene(loader.getRoot()));
        controller.setPreviousController(this);
        controller.show();

    }







}
