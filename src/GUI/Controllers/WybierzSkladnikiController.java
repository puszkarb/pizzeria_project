package GUI.Controllers;

import com.jfoenix.controls.JFXButton;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import Products.ControlOfOrder;

public class WybierzSkladnikiController extends BaseController {

    @FXML
    private JFXButton serButton;

    @FXML
    private JFXButton szynkaButtton;

    @FXML
    private JFXButton ananasButton;

    @FXML
    private JFXButton pieczarkiButton;

    @FXML
    private JFXButton bekonButton;

    @FXML
    private JFXButton kukurydzaButton;

    @FXML
    private JFXButton pomidoryButton;

    @FXML
    private CheckBox serCheckBox;

    @FXML
    private CheckBox szynkaCheckBox;

    @FXML
    private CheckBox bekonCheckBox;

    @FXML
    private CheckBox ananasCheckBox;

    @FXML
    private CheckBox pieczarkiCheckBox;

    @FXML
    private CheckBox kukurydzaCheckBox;

    @FXML
    private CheckBox pomidoryCheckBox;



    public static ControlOfOrder getControllerOfOrder() {
        return controllerOfOrder;
    }



    public void DostosujSkladniki( ActionEvent event )
    {
        if ( event.getSource().equals((serButton)))
        {
            serCheckBox.setSelected(!serCheckBox.isSelected());
        }
        else if(event.getSource().equals(szynkaButtton)) {
            szynkaCheckBox.setSelected(!szynkaCheckBox.isSelected());
        }
        else if(event.getSource().equals(bekonButton)) {
            bekonCheckBox.setSelected(!bekonCheckBox.isSelected());
        }
        else if(event.getSource().equals(ananasButton)) {
            ananasCheckBox.setSelected(!ananasCheckBox.isSelected());
        }
        else if(event.getSource().equals(pieczarkiButton)) {
            pieczarkiCheckBox.setSelected(!pieczarkiCheckBox.isSelected());
        }
        else if(event.getSource().equals(kukurydzaButton)) {
            kukurydzaCheckBox.setSelected(!kukurydzaCheckBox.isSelected());
        }
        else if(event.getSource().equals(pomidoryButton)) {
            pomidoryCheckBox.setSelected(!pomidoryCheckBox.isSelected());
        }

    }

    public void NextScene() throws java.io.IOException
    {
        controllerOfOrder.addExtraIngredients(serCheckBox.isSelected(), szynkaCheckBox.isSelected(),
                bekonCheckBox.isSelected(), ananasCheckBox.isSelected(), pieczarkiCheckBox.isSelected(), kukurydzaCheckBox.isSelected(), pomidoryCheckBox.isSelected());

        controllerOfOrder.addPizzaToOrder();


        final FXMLLoader loader = new FXMLLoader(getClass().getResource("../gotowe.fxml"));
        loader.load();
        final BaseController controller = loader.getController();
        controller.configure(application, new Scene(loader.getRoot()));
        controller.setPreviousController(this);
        controller.show();
    }

}
