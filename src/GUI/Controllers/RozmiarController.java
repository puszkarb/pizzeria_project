package GUI.Controllers;

import com.jfoenix.controls.JFXButton;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import Products.ControlOfOrder;

public class RozmiarController extends BaseController {



    public static ControlOfOrder getControllerOfOrder() {
        return controllerOfOrder;
    }


    @FXML
    private JFXButton malaPizzaButton;
    @FXML
    private JFXButton sredniaPizzaButton;
    @FXML
    private JFXButton duzaPizzaButton;

    public void wybierzRozmiar ( ActionEvent event ) throws java.io.IOException
    {
        if ( event.getSource().equals((malaPizzaButton)))
        {
            controllerOfOrder.PizzaSetSize(20);
        }
        else if(event.getSource().equals(sredniaPizzaButton)){
            controllerOfOrder.PizzaSetSize(30);

        }else if(event.getSource().equals(duzaPizzaButton)){
            controllerOfOrder.PizzaSetSize(40);
        }
        final FXMLLoader loader = new FXMLLoader(getClass().getResource("../Ciasto.fxml"));
        loader.load();
        final BaseController controller = loader.getController();
        controller.configure(application, new Scene(loader.getRoot()));
        controller.setPreviousController(this);
        controller.show();
    }
}
