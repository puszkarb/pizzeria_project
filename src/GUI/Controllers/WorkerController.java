package GUI.Controllers;

import GUI.Main;
import DBControl.ObslugaBD;
import com.jfoenix.controls.JFXButton;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import Products.ControlOfOrder;
import Products.Order;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class WorkerController extends BaseController {


    private Main application;
    private Scene scene;



    @FXML
    private TableView<Order> tableView;

    @FXML
    private TableColumn<Order,String> idColumn;
    @FXML
    private TableColumn<Order, String> pizzaColumn;
    @FXML
    private TableColumn<Order, String> sosyColumn;
    @FXML
    private TableColumn<Order, String> drinkColumn;
    @FXML
    private TableColumn<Order, String> commentColumn;
    @FXML
    private TableColumn<Order, String> statusColumn;
    @FXML
    private JFXButton loadButton;
    @FXML
    private JFXButton closeButton;

    private ObservableList<Order> data;
    private ObslugaBD bd;

    static final ControlOfOrder controllerOfOrder = new ControlOfOrder();


    public void initialize(URL location, ResourceBundle resources) {
        bd = new ObslugaBD();


    }

    Connection connection;

    @FXML
    private void loadDataFromDatabase() {
        System.out.println("W loadDatabase");
        bd = new ObslugaBD();

        connection = bd.polaczZBaza();
        data = FXCollections.observableArrayList();

        //in progress by PUSZKARSKI !!! --- pls don;t touch XD
        // poprzedni comit miał beznadziejny komentarz sorka

        try {
            ResultSet resultSet = connection.createStatement().executeQuery("SELECT * FROM zamowienia");

            while (resultSet.next()) {
                data.add(new Order(resultSet.getInt(1),resultSet.getString(2), resultSet.getString(9), resultSet.getString(8)));

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        idColumn.setCellValueFactory(new PropertyValueFactory<>("order_id_Property"));
        pizzaColumn.setCellValueFactory(new PropertyValueFactory<>("nameProperty"));
        commentColumn.setCellValueFactory(new PropertyValueFactory<>("commentProperty"));
        statusColumn.setCellValueFactory(new PropertyValueFactory<>("statusProperty"));

        tableView.setRowFactory( tv -> {
            TableRow<Order> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                    Order rowData = row.getItem();

                    updateData("status", rowData, changeStatus(rowData.getStatus()));

                    loadDataFromDatabase();
                    System.out.println(rowData);
                }
            });
            return row ;
        });


        tableView.setItems(null);
        tableView.setItems(data);
    }

    private void updateData(String status, Order rowData, String s) {
        try {
            int id = rowData.getId();
            PreparedStatement stmt = connection.prepareStatement("UPDATE zamowienia SET "+status+" = ? WHERE id_zamowienia = ? ");
            stmt.setString(1, s);
            stmt.setString(2, Integer.toString(id));
            stmt.execute();
        } catch (SQLException e) {
            System.out.println("Cos poszło nei tak");
            e.printStackTrace();
        }
    }

    private String changeStatus(String actualStatus){
        if(actualStatus.equalsIgnoreCase("przyjete")){
            return "realizowane";
        }else if (actualStatus.equalsIgnoreCase("realizowane")){
            return "dostarczane";
        }else if (actualStatus.equalsIgnoreCase("dostarczane")){
            return "zrealizowane";
        }else if (actualStatus.equalsIgnoreCase("zrealizowane")){
            return "anulowane";
        }else if (actualStatus.equalsIgnoreCase("anulowane")) {
            return "przyjete";
        }
        return "anulowane";

   }

   @FXML
    public void closeApp(){
       Stage stage = (Stage) closeButton.getScene().getWindow();
       // do what you have to do
       stage.close();

   }


}
