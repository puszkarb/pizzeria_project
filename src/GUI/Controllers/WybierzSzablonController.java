package GUI.Controllers;

import com.jfoenix.controls.JFXButton;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import Products.ControlOfOrder;

public class WybierzSzablonController extends BaseController {



    public static ControlOfOrder getControllerOfOrder() {
        return controllerOfOrder;
    }

    @FXML
    private JFXButton pizzaStudenckaButton;
    @FXML
    private JFXButton pizzaWezuwiuszButton;
    @FXML
    private JFXButton pizzaNowowiejskaButton;
    @FXML
    private JFXButton pizzaElektryzujacaButton;




    public void wybierzSzablon( ActionEvent event )   throws java.io.IOException
    {
        if ( event.getSource().equals((pizzaStudenckaButton)))
        {
            controllerOfOrder.createPizza("Studencka");
        }
        else if(event.getSource().equals(pizzaWezuwiuszButton)) {
            controllerOfOrder.createPizza("Wezuwiusz");
        }
        else if(event.getSource().equals(pizzaNowowiejskaButton)) {
            controllerOfOrder.createPizza("Nowowiejska");
        }
        else if(event.getSource().equals(pizzaElektryzujacaButton)) {
            controllerOfOrder.createPizza("Elektryzujaca");
        }

        final FXMLLoader loader = new FXMLLoader(getClass().getResource("../wybierzSkladniki.fxml"));
        loader.load();
        final BaseController controller = loader.getController();
        controller.configure(application, new Scene(loader.getRoot()));
        controller.setPreviousController(this);
        controller.show();
    }
}
