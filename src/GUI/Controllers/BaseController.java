package GUI.Controllers;

import GUI.Main;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import Products.ControlOfOrder;

public class BaseController {

    protected Main application;
    protected Scene scene;
    static final ControlOfOrder controllerOfOrder = new ControlOfOrder();
    BaseController previousController;

    public void configure(final Main application, final Scene scene) {
        this.application = application;
        this.scene = scene;
    }


    public void setPreviousController(final BaseController previousController) {
        this.previousController = previousController;
    }

    public void show() {
        application.show(scene);
    }

    public void doGlownej()
    {
        application.controller1.show();
    }

    public void kolejnaPizza() throws java.io.IOException
    {
        final FXMLLoader loader = new FXMLLoader(getClass().getResource("../Rozmiar.fxml"));
        loader.load();
        final BaseController controller = loader.getController();
        controller.configure(application, new Scene(loader.getRoot()));
        controller.show();
    }


    public void backToPreviousScene()
    {
        previousController.show();
    }
}
