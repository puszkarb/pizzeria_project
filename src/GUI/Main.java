package GUI;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;

public class Main extends Application {
    public Controller controller1;
    public Controller controller2;
    public Controller controller3;
    public Controller controller4;
    public Controller controller5;
    public Controller controller6;
    public Controller controller7;
    public Controller controller8;
    public Controller controller9;
    public Controller controllerW;
    private Stage primaryStage;

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(final Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        controller1 = load("Main.fxml");  //laduje sceny
        controller2 = load("Rejestracja.fxml");
        //controller3 = load("Rozmiar.fxml");
        //controller4 = load("Ciasto.fxml");
        controller5 = load("Zarejestrowano.fxml");
        controller6 = load("EmailDuplikat.fxml");
        controller7 = load("nieZalogowano.fxml");
       // controller8 = load("wybierzSzablon.fxml");
      //  controller9 = load("wybierzSkladniki.fxml");
             // controller9 = load("wybierzInneProdukty.fxml");
      //  controllerW = load("Worker.fxml");

        controller1.doGlownej();

        primaryStage.initStyle(StageStyle.DECORATED);   //czyli z paskiem systemowym
        primaryStage.setTitle("ElektrycznySystemZamawianiaPizzy");
        primaryStage.setResizable(false);   //nie mozna zmienic wielkosci okienka
        primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("logo.png"))); //ikonka

        primaryStage.show();

    }

    public Controller load(final String name) throws IOException {
        final FXMLLoader loader = new FXMLLoader(getClass().getResource(name));
        loader.load();
        final Controller controller = loader.getController();
        controller.configure(this, new Scene(loader.getRoot()));
        return controller;
    }


    public void show(final Scene scene) {
        primaryStage.setScene(scene);
    }
}