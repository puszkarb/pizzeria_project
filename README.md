# Pizzeria_Project

## Description
Application which comprehensively handle orders flow in gastro company -  pizzeria is given example in this case. User can order his meal, meanwhile company staff is able to  check and update its status in database.
Both Worker and User are able to log in through the same initial interface, yet they final environment in which groups will operate differs. 

### Technologies used
	Database: MySQL       
	Language: Java            
	GUI: JavaFX        
	Tests: JUnit 5           

## Installation / First Use  ######
1) Create MySQL database "pizzeria" with user "pizzeria" and password "pizzeria"                                    
2) Run db_users_creator.sql and db_orders_creator.sql in MySQL terminal                                    
3) Open whole project with chosen software i.e. InteliJ              
4) Configure whole project in "Project Structure":            
	a) Tag "src" as source directory, "out" as output directory and "Tests" as test directory                  
	b) Add module dependencies :             
		- jfoenix-9.0.1.jar ( located in src/GUI/ )                    
		- mysql-connector-java-5.1.46.jar ( located in src/DBControl )                  
5) Add JUnit 5 in test classes ( InteliJ will force you to do that, after that JUnit has to be also included in module dependencies )                     
6) Run ServerMain.java              
7) Run ClientTest.java ( located in server_client package)              

##### Usage

1) Create new User with "Zarejestruj" button.               
2) Make orders and send them to the sever with "zakoncz" button at the final stage              
                       
However, if Worker's view is desired one, when login stage is achieved fill fields with credentials:     
	e-mail: worker         
	password: worker          

then click "Zaladuj" button to connect to the database and download all data from it.
When clicked, fields in "status" column will change their state (this are preparation process consecutive steps) - everything is also changed in database.

Enjoy :)
