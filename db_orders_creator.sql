create table pizzeria.zamowienia(
id_zamowienia int not null AUTO_INCREMENT ,
primary key(id_zamowienia),
pizza_sklad nvarchar(2001) null,
sosy varchar(30) null,
napoje nvarchar(100) null,
ulica varchar(60) not null,
telefon varchar(9) not null,
email_zamawiacza varchar(30) null,
FOREIGN KEY (email_zamawiacza) references dane_klientow(email),
status varchar(30) not null,
komentarz varchar(150) null,
iloscPkt int null,
cena double not null
);

